import React, { useState } from 'react';
import {registerUser} from '../../api/User.api';


const RegisterForm = props => {


    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [registerError, setRegisterError] = useState('');


    const onRegisterClicked = async ev => {
        setIsLoading(true);
        let result;
        try {
            const {status}  = await registerUser(username, password);
            result = status === 201;
        } catch (e) {
            setRegisterError(e.message || e);
        } finally {
            setIsLoading(false);
            props.complete(result);
        }

    }

    const onUsernameChange = ev => setUsername(ev.target.value);
    const onPasswordChange = ev => setPassword(ev.target.value);

    return (
        <form>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Enter a username" onChange={onUsernameChange} ></input>
            </div>
            <div>
                <label>Password: </label>
                <input type="password" placeholder="Enter a password" onChange={onPasswordChange} ></input>
            </div>

            <div>
                <button type="button" onClick={onRegisterClicked}>Register</button>
            </div>


            { isLoading && <div>Registering user...</div> }

            { registerError && <div>{registerError}</div> }



        </form>
    )
}

export default RegisterForm;
