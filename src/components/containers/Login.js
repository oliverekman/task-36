import React from 'react';
import LoginForm from '../forms/LoginForm';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';

const Login = () => {
    return (
        <div>
            <h1>Login to Survey</h1>

            <LoginForm></LoginForm>

            <Link to="/register" >
                No account? Register here
            </Link>
        </div>
    )
};

export default Login;