import React from 'react';
import RegisterForm from '../forms/RegisterForm';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import { useHistory } from 'react-router-dom/cjs/react-router-dom';

const Register = () => {

    const history = useHistory();


    const handleRegisterComplete = (result) => {
        console.log('Triggered from RegisterForm', result);
        if (result) {
            history.replace("/dashboard");
        }
    };

    return (

        <div>
            <h1>Register for Survey</h1>

            <RegisterForm complete={ handleRegisterComplete }></RegisterForm>

            <Link to="/login" > 
            Already registered? Login here 
            </Link>
        </div>

    );

};

export default Register;