import React from 'react';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';

const NotFound = () => (
    <div>
        <h1>Page not found</h1>

        <Link to="/login" >
            Go Back Home
        </Link>
        
    </div>
);

export default NotFound;