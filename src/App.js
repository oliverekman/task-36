import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';



import Register from './components/containers/Register';
import Login from './components/containers/Login';
import Dashboard from './components/containers/Dashboard';
import NotFound from './components/containers/NotFound';

function App() {
  return (

    <Router>
      <div className="App">
        My React App
        <Switch>

      <Route path="/login" component={ Login } />
      <Route path="/register" component={ Register } />
      <Route path="/dashboard" component={ Dashboard } />
      <Route path="*" component={ NotFound } />
      </Switch>
      </div>
    </Router>

  );
}

export default App;
